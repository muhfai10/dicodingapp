package com.example.muhfai10.dicodingapp.view;

/**
 * Created by MuhFai10 on 11/10/2019.
 */

public interface BasePresenter {

    void start();

}