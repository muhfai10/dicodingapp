package com.example.muhfai10.dicodingapp.view;

import android.app.Activity;
import android.content.Intent;
import android.os.StrictMode;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.example.muhfai10.dicodingapp.R;
import com.example.muhfai10.dicodingapp.view.home.MainActivity;

public class MySplashActivity extends Activity {

    private final int SECONDS = 3;
    private TextView tvLoading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_my_splash);

        tvLoading = (TextView) findViewById(R.id.loading_text);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        final Thread thread = new Thread(new Loading());
        thread.start();
    }
    class Loading implements Runnable {
        @Override
        public void run() {

            for(int i=0; i<=SECONDS; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            final Intent intent = new Intent();
            intent.setClass(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }

    }
}
