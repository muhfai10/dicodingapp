package com.example.muhfai10.dicodingapp.view.about;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.muhfai10.dicodingapp.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
