package com.example.muhfai10.dicodingapp.view.detail;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.muhfai10.dicodingapp.R;

public class DetailActivity extends AppCompatActivity {

    private TextView tvNama;
    private TextView tvDeskripsi;
    private TextView tvTTLahir;
    private TextView tvAlamat;
    private TextView tvJudulTA, tvFirstSkill, tvSecondSkill, tvThirdSkill,
            tvHobby, tvMottoHidup;
    private ImageView ivProfilePhoto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_detail);
        //getActionBar().setDisplayHomeAsUpEnabled(true);

        tvNama = findViewById(R.id.tvProfileName);
        tvDeskripsi = findViewById(R.id.tvDeskripsi);
        tvTTLahir = findViewById(R.id.tvTTLahir);
        tvAlamat = findViewById(R.id.tvAlamat);
        tvJudulTA = findViewById(R.id.tvJudulTA);
        tvFirstSkill = findViewById(R.id.tvFirstSkill);
        tvSecondSkill = findViewById(R.id.tvSecondSkill);
        tvThirdSkill = findViewById(R.id.tvThirdSkill);
        tvHobby = findViewById(R.id.tvHobby);
        tvMottoHidup = findViewById(R.id.tvMotto);
        ivProfilePhoto = findViewById(R.id.ivProfilePhoto);
        tvNama.setText(getIntent().getStringExtra("namaProfile"));
        tvDeskripsi.setText(getIntent().getStringExtra("deskripsiProfile"));
        tvTTLahir.setText(getIntent().getStringExtra("ttLahir"));
        tvAlamat.setText(getIntent().getStringExtra("alamat"));
        tvJudulTA.setText(getIntent().getStringExtra("judulTA"));
        tvFirstSkill.setText(getIntent().getStringExtra("firstSkill"));
        tvSecondSkill.setText(getIntent().getStringExtra("secondSkill"));
        tvThirdSkill.setText(getIntent().getStringExtra("thirdSkill"));
        tvHobby.setText(getIntent().getStringExtra("hobby"));
        tvMottoHidup.setText(getIntent().getStringExtra("mottoHidup"));
        //int fotoURL = savedInstanceState.getInt("fotoURL");
        //ivProfilePhoto.setImageResource(fotoURL);
        //String fotoURL = getIntent().getExtras().getString("fotoURL");
        Glide.with(this).load(getIntent().getIntExtra("fotoURL",0)).into(ivProfilePhoto);

    }
}
