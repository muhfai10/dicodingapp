package com.example.muhfai10.dicodingapp.view;

/**
 * Created by MuhFai10 on 11/10/2019.
 */

public interface BaseView<T> {

    void setPresenter(T presenter);

}