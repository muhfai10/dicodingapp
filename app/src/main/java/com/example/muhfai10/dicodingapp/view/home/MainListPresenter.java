package com.example.muhfai10.dicodingapp.view.home;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;

import com.example.muhfai10.dicodingapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MuhFai10 on 11/10/2019.
 */

public class MainListPresenter implements MainListContract.Presenter {

    private final MainListContract.View mView;
    private boolean firstLoad=true;
    //private Context mContext;

    public MainListPresenter(@NonNull MainListContract.View mView) {
        this.mView = mView;
        //mContext = context;
        mView.setPresenter(this);
    }

    @Override
    public void loadProfileList(boolean forceUpdate) {
        List<ProfileDto> profileDtoList = getDummyData();
        mView.showProfileList(profileDtoList);
    }

    @Override
    public void start() {
        loadProfileList(true);
    }

    private List<ProfileDto> getDummyData(){
        List<ProfileDto> profileDtoList = new ArrayList<>();

        ProfileDto profileDto = new ProfileDto();
        profileDto.setNama("Muhammad Faiz");
        profileDto.setDeskripsiDiri("Skilled in JAVA Framework (Spring MVC).");
        profileDto.setTtLahir("Padang, 24 Desember 1997");
        profileDto.setAlamat("Perumahan Cemara CC 6 RT 01 / RW 03");
        profileDto.setJudulTA("Sistem Informasi Evaluasi Kinerja Dosen Berbasis Java menggunakan Spring Framework");
        profileDto.setFotoURL(R.drawable.faiz);
        profileDto.setFirstSkill("JAVA");
        profileDto.setSecondSkill("Android Development");
        profileDto.setThirdSkill("Web Development");
        profileDto.setHobby("Futsal, Musik dan Ngoding.");
        profileDto.setMottoHidup("Ubah pikiranmu dan kau dapat mengubah duniamu.");
        profileDtoList.add(profileDto);

        profileDto = new ProfileDto();
        profileDto.setNama("Nurmala Sagita Suryadi");
        profileDto.setDeskripsiDiri("Skilled in Public Speaking.");
        profileDto.setTtLahir("Bogor, 5 Juli 1997");
        profileDto.setAlamat("Perumahan Cemara CC 5 RT 01 / RW 03");
        profileDto.setJudulTA("Sistem Informasi Surat Masuk Berbasis Web menggunakan PHP");
        profileDto.setFotoURL(R.drawable.gita);
        profileDto.setFirstSkill("Public Speaking");
        profileDto.setSecondSkill("IT Consulting");
        profileDto.setThirdSkill("Web Development");
        profileDto.setHobby("Membaca, Menulis dan Ngoding.");
        profileDto.setMottoHidup("Jika kamu ingin hidup bahagia, terikatlah pada tujuan, bukan orang atau benda.");
        profileDtoList.add(profileDto);

        profileDto = new ProfileDto();
        profileDto.setNama("Dio Nanda");
        profileDto.setDeskripsiDiri("Skilled in PHP Framework (Yii2).");
        profileDto.setTtLahir("Padang, 28 Mei 1998");
        profileDto.setAlamat("Jalan Nias Utara CC 5 RT 02 / RW 03");
        profileDto.setJudulTA("Sistem Informasi Pengelolaan Data Tugas Akhir Berbasis Web menggunakan PHP");
        profileDto.setFotoURL(R.drawable.dio);
        profileDto.setFirstSkill("PHP");
        profileDto.setSecondSkill("Android Development");
        profileDto.setThirdSkill("Web Development");
        profileDto.setHobby("Membaca dan Ngoding.");
        profileDto.setMottoHidup("Terlalu memperdulikan apa yang orang pikirkan dan kau akan selalu menjadi tahanan mereka.");
        profileDtoList.add(profileDto);

        profileDto = new ProfileDto();
        profileDto.setNama("Febri Wahyuni");
        profileDto.setDeskripsiDiri("Skilled in PHP Framework (CI).");
        profileDto.setTtLahir("Padang, 13 Februari 1998");
        profileDto.setAlamat("Komplek Cendana BB 02 RT 02 / RW 03");
        profileDto.setJudulTA("Sistem Informasi Penjualan Barang Berbasis Web menggunakan PHP");
        profileDto.setFotoURL(R.drawable.feby);
        profileDto.setFirstSkill("PHP");
        profileDto.setSecondSkill("CI");
        profileDto.setThirdSkill("Web Development");
        profileDto.setHobby("Tata rias, Membaca dan Public Speaking");
        profileDto.setMottoHidup("Terlalu memperdulikan apa yang orang pikirkan dan kau akan selalu menjadi tahanan mereka.");
        profileDtoList.add(profileDto);

        profileDto = new ProfileDto();
        profileDto.setNama("Tasrik Rizaldi");
        profileDto.setDeskripsiDiri("Skilled in Java Framework (Struts).");
        profileDto.setTtLahir("Padang, 08 April 1998");
        profileDto.setAlamat("Komplek Arai Pinang BB 09 RT 02 / RW 03");
        profileDto.setJudulTA("Sistem Informasi Pengelolaan Data TPA Berbasis Web menggunakan Struts");
        profileDto.setFotoURL(R.drawable.tasrik);
        profileDto.setFirstSkill("Java");
        profileDto.setSecondSkill("Struts");
        profileDto.setThirdSkill("Web Development");
        profileDto.setHobby("Bermain game, Membaca dan Ngoding");
        profileDto.setMottoHidup("Jika kau tak suka sesuatu, ubahlah. Jika tak bisa, maka ubahlah cara pandangmu tentangnya.");
        profileDtoList.add(profileDto);

        profileDto = new ProfileDto();
        profileDto.setNama("Octa Shela Karlin Bukit");
        profileDto.setDeskripsiDiri("Skilled in PHP Framework (CI).");
        profileDto.setTtLahir("Padang Sidempuan, 20 Oktober 1998");
        profileDto.setAlamat("Komplek Katapiang BB 02 RT 06 / RW 03");
        profileDto.setJudulTA("Sistem Informasi Pengelolaan Data PKL Berbasis Web menggunakan CI");
        profileDto.setFotoURL(R.drawable.octa);
        profileDto.setFirstSkill("PHP");
        profileDto.setSecondSkill("CI");
        profileDto.setThirdSkill("Web Development");
        profileDto.setHobby("Tata rias, Memasak dan Ngoding");
        profileDto.setMottoHidup("Waktumu terbatas, jangan habiskan dengan mengurusi hidup orang lain.");
        profileDtoList.add(profileDto);

        profileDto = new ProfileDto();
        profileDto.setNama("Nurul Alawiyah Dalimunthe");
        profileDto.setDeskripsiDiri("Skilled in PHP Framework (CI).");
        profileDto.setTtLahir("Padang Sidempuan, 13 Januari 1999");
        profileDto.setAlamat("Komplek Katapiang BB 02 RT 06 / RW 03");
        profileDto.setJudulTA("Sistem Informasi Pengelolaan Acara Berbasis Web menggunakan CI");
        profileDto.setFotoURL(R.drawable.nurul);
        profileDto.setFirstSkill("PHP");
        profileDto.setSecondSkill("CI");
        profileDto.setThirdSkill("Web Development");
        profileDto.setHobby("Travelling, Memasak dan Ngoding");
        profileDto.setMottoHidup("Sukses adalah saat persiapan dan kesempatan bertemu.");
        profileDtoList.add(profileDto);

        profileDto = new ProfileDto();
        profileDto.setNama("Bayu Okta Dharma");
        profileDto.setDeskripsiDiri("Skilled in PHP Framework (CI).");
        profileDto.setTtLahir("Sijunjung, 20 Oktober 1998");
        profileDto.setAlamat("Perumahan Kuranji CC 09 RT 04 / RW 03");
        profileDto.setJudulTA("Sistem Informasi Sekolah Berbasis Web menggunakan CI");
        profileDto.setFotoURL(R.drawable.bod);
        profileDto.setFirstSkill("PHP");
        profileDto.setSecondSkill("CI");
        profileDto.setThirdSkill("Web Development");
        profileDto.setHobby("Berpetualang, Musik dan Ngoding");
        profileDto.setMottoHidup("Kesempatan bukanlah hal yang kebetulan. Kamu harus menciptakannya.");
        profileDtoList.add(profileDto);

        profileDto = new ProfileDto();
        profileDto.setNama("Aldo Febriano Caraka");
        profileDto.setDeskripsiDiri("Skilled in Android Development.");
        profileDto.setTtLahir("Padang, 12 Februari 1998");
        profileDto.setAlamat("Perumahan Jati FF 03 RT 04 / RW 03");
        profileDto.setJudulTA("Aplikasi Ujian Online di SMK 6 Padang Berbasis Android ");
        profileDto.setFotoURL(R.drawable.aldo);
        profileDto.setFirstSkill("Android Development");
        profileDto.setSecondSkill("Java");
        profileDto.setThirdSkill("Web Development");
        profileDto.setHobby("Bermain game, Menulis dan Ngoding");
        profileDto.setMottoHidup("Jangan biarkan hari kemarin merenggut banyak hal hari ini.");
        profileDtoList.add(profileDto);

        profileDto = new ProfileDto();
        profileDto.setNama("Jufri Chory Putri");
        profileDto.setDeskripsiDiri("Skilled in Javascript Web Development.");
        profileDto.setTtLahir("Padang, 23 Januari 1998");
        profileDto.setAlamat("Perumahan Pasar Baru DD 03 RT 04 / RW 03");
        profileDto.setJudulTA("Sistem Informasi Monitoring Ayam Berbasis Web Menggunakan Javascript");
        profileDto.setFotoURL(R.drawable.oyi);
        profileDto.setFirstSkill("Javascript");
        profileDto.setSecondSkill("PHP");
        profileDto.setThirdSkill("Web Development");
        profileDto.setHobby("Bermain game, Tata Rias dan Ngoding");
        profileDto.setMottoHidup("Rahasia kesuksesan adalah mengetahui yang orang lain tidak tahu.");
        profileDtoList.add(profileDto);

        profileDto = new ProfileDto();
        profileDto.setNama("Rizky Tri Demarwan");
        profileDto.setDeskripsiDiri("Skilled in Arduino IOT.");
        profileDto.setTtLahir("Padang, 19 Januari 1998");
        profileDto.setAlamat("Perumahan Pasar Baru DD 03 RT 04 / RW 03");
        profileDto.setJudulTA("Aplikasi Monitoring Hewan Ternak Berbasis Android");
        profileDto.setFotoURL(R.drawable.gembs);
        profileDto.setFirstSkill("Arduino");
        profileDto.setSecondSkill("Android Development");
        profileDto.setThirdSkill("Web Development");
        profileDto.setHobby("Bermain game, Hang-Out dan Ngoding");
        profileDto.setMottoHidup("Jangan menunggu. Takkan pernah ada waktu yang tepat.");
        profileDtoList.add(profileDto);

        return profileDtoList;

    }
}
