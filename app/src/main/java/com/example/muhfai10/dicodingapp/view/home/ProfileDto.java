package com.example.muhfai10.dicodingapp.view.home;

/**
 * Created by MuhFai10 on 11/10/2019.
 */

public class ProfileDto {
    private String nama;
    private String deskripsiDiri;
    private String ttLahir;
    private String alamat;
    private String judulTA;
    private Integer fotoURL;
    private String firstSkill;
    private String secondSkill;
    private String thirdSkill;
    private String hobby;
    private String mottoHidup;


    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsiDiri() {
        return deskripsiDiri;
    }

    public void setDeskripsiDiri(String deskripsiDiri) {
        this.deskripsiDiri = deskripsiDiri;
    }

    public String getTtLahir() {
        return ttLahir;
    }

    public void setTtLahir(String ttLahir) {
        this.ttLahir = ttLahir;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public Integer getFotoURL() {
        return fotoURL;
    }

    public void setFotoURL(Integer fotoURL) {
        this.fotoURL = fotoURL;
    }

    public String getFirstSkill() {
        return firstSkill;
    }

    public void setFirstSkill(String firstSkill) {
        this.firstSkill = firstSkill;
    }

    public String getSecondSkill() {
        return secondSkill;
    }

    public void setSecondSkill(String secondSkill) {
        this.secondSkill = secondSkill;
    }

    public String getThirdSkill() {
        return thirdSkill;
    }

    public void setThirdSkill(String thirdSkill) {
        this.thirdSkill = thirdSkill;
    }

    public String getJudulTA() {
        return judulTA;
    }

    public void setJudulTA(String judulTA) {
        this.judulTA = judulTA;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getMottoHidup() {
        return mottoHidup;
    }

    public void setMottoHidup(String mottoHidup) {
        this.mottoHidup = mottoHidup;
    }
}
