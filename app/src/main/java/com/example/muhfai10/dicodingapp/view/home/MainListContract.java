package com.example.muhfai10.dicodingapp.view.home;

import com.example.muhfai10.dicodingapp.view.BasePresenter;
import com.example.muhfai10.dicodingapp.view.BaseView;

import java.util.List;

/**
 * Created by MuhFai10 on 11/10/2019.
 */

public interface MainListContract {
    interface View extends BaseView<Presenter> {
        void showProfileList(List<ProfileDto> profileDtoList);
    }
    interface Presenter extends BasePresenter {
        void loadProfileList(boolean forceUpdate);
    }
}
