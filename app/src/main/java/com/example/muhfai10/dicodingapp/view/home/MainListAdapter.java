package com.example.muhfai10.dicodingapp.view.home;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.muhfai10.dicodingapp.R;
import com.example.muhfai10.dicodingapp.view.detail.DetailActivity;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by MuhFai10 on 11/10/2019.
 */

public class MainListAdapter extends RecyclerView.Adapter<MainListAdapter.MainListViewHolder> {

    private List<ProfileDto> mProfileDtoList;
    private Context mContext;
    MainListAdapter (List<ProfileDto> ProfileDtoList,Context context){
        mProfileDtoList = ProfileDtoList;
        mContext = context;
    }
    public void updateData(List<ProfileDto> profileDtoList){
        mProfileDtoList = profileDtoList;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public MainListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile,parent,false);
        return new MainListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainListViewHolder holder, final int position) {
        final ProfileDto profileDto = mProfileDtoList.get(position);
        holder.tvNama.setText(profileDto.getNama());
        holder.tvDeskripsi.setText(profileDto.getDeskripsiDiri());
        holder.tvTTLahir.setText(profileDto.getTtLahir());
        Glide.with(mContext).load(profileDto.getFotoURL()).into(holder.ivProfilePhoto);
        holder.cvContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,DetailActivity.class);
                intent.putExtra("namaProfile",profileDto.getNama());
                intent.putExtra("deskripsiProfile",profileDto.getDeskripsiDiri());
                intent.putExtra("ttLahir",profileDto.getTtLahir());

                intent.putExtra("fotoURL", profileDto.getFotoURL());
                intent.putExtra("alamat",profileDto.getAlamat());
                intent.putExtra("judulTA",profileDto.getJudulTA());
                intent.putExtra("firstSkill",profileDto.getFirstSkill());
                intent.putExtra("secondSkill",profileDto.getSecondSkill());
                intent.putExtra("thirdSkill",profileDto.getThirdSkill());
                intent.putExtra("hobby",profileDto.getHobby());
                intent.putExtra("mottoHidup",profileDto.getMottoHidup());
                mContext.startActivity(intent);
            }
        });

        //Intent intent = new Intent(mContext, DetailActivity.class);
    }

    @Override
    public int getItemCount() {
        return mProfileDtoList.size();
    }

    class MainListViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNama;
        private TextView tvDeskripsi;
        private TextView tvTTLahir;
        private ImageView ivProfilePhoto;
        private CardView cvContent;

        public MainListViewHolder(View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvProfileName);
            tvDeskripsi = itemView.findViewById(R.id.tvDeskripsi);
            tvTTLahir = itemView.findViewById(R.id.tvTTLahir);
            ivProfilePhoto = itemView.findViewById(R.id.ivProfilePhoto);
            cvContent = itemView.findViewById(R.id.cvContent);
        }
    }
}
