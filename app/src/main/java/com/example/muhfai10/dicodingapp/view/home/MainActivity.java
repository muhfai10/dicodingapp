package com.example.muhfai10.dicodingapp.view.home;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.muhfai10.dicodingapp.R;
import com.example.muhfai10.dicodingapp.view.about.AboutActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainListContract.View{

    private MainListContract.Presenter mPresenter;
    private MainListAdapter mListAdapter;
    private RecyclerView rvProfileList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListAdapter = new MainListAdapter(new ArrayList<ProfileDto>(0),this);
        rvProfileList = findViewById(R.id.rvProfileList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvProfileList.setLayoutManager(linearLayoutManager);
        rvProfileList.setAdapter(mListAdapter);

        mPresenter = new MainListPresenter(this);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miProfile :
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProfileList(List<ProfileDto> profileDtoList) {
        mListAdapter.updateData(profileDtoList);
        rvProfileList.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.start();
    }

    @Override
    public void setPresenter(MainListContract.Presenter presenter) {
        mPresenter = presenter;
    }
}
